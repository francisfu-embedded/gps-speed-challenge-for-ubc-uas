#Imported modules
import socket
import time
import utm

#Host name and port number definitions
HOST_NAME = '127.0.0.1'
PORT_NUM = 1024

#Some global variables
utm_easting_now = 0
utm_northing_now = 0

utm_easting_last = 0
utm_northing_last = 0

utm_easting_original = 0
utm_northing_original = 0

counter = 0

#Create client sockets and connect to the gps data server
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect((HOST_NAME, PORT_NUM))

while True:
    #Message receive and decode to utf-8 form
    msg = s.recv(128)
    decoded_msg = msg.decode('utf-8')

    #End message listening if signal "End" is received
    if counter >= 42:
        s.close()
        break
    
    #Split the coordinate string into two floating point values
    coordinate = decoded_msg.split()

    #Use the function from_latlon from the UTM library to perform coordinate conversion
    coordinate_in_utm = ()
    coordinate_in_utm = utm.from_latlon(float(coordinate[0]),float(coordinate[1]))

    utm_easting_now = coordinate_in_utm[0]
    utm_northing_now = coordinate_in_utm[1]

    #Only store last and original east/northings for the first message received
    if counter == 0:
        utm_easting_original = utm_easting_now
        utm_northing_original = utm_northing_now
        utm_easting_last = utm_easting_now
        utm_northing_last = utm_northing_now
    else:
        #Speed = sqrt(d1^2 +d2^2)/time
        #In which d1 and d2 are instantaneous displacement in meters
        #in easting and northing 
        #And time is 1 second
        east_dis = utm_easting_now - utm_easting_last
        north_dis = utm_northing_now - utm_northing_last
        speed = round((east_dis**2 + north_dis**2)**0.5,2)

        #for average speed, d1 is position right now minus position at t = 0
        east_dis = utm_easting_now - utm_easting_original
        north_dis = utm_northing_now - utm_northing_original
        avr_speed = round(((east_dis**2 + north_dis**2)**0.5)/counter,2)
        
        utm_easting_last = utm_easting_now
        utm_northing_last = utm_northing_now
        
        print("Real Time Instantaneous Speed: " + str(speed) + ' m/s | Average Speed: ' + str(avr_speed) )

    #increment the counter
    counter += 1
        

