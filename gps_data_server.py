#Imported modules
import socket
import time
from signal import signal, SIGPIPE, SIG_DFL

#disable error "Broken Pipe"
signal(SIGPIPE, SIG_DFL) 

#Host name and port number definitions
HOST_NAME = '127.0.0.1'
PORT_NUM = 1024

#Open up the GPS file to aquire gps data. Normally the flight controller should get the data from its GPS module
#Then send to our atennas via its RF module in real time
file = open('gps_coordinates.txt','r')

#Create and bind socket to local IP and the port name
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind((HOST_NAME,PORT_NUM))
s.listen(5)

while True:
    #Now our endpoint knows about the OTHER endpoint and prints the confirmation
    clientsocket, address = s.accept()
    print(f"Connection from {address} has been established.")
        
    for i in range(42):
        #Service the data once per second
        time.sleep(1)
        line = file.readline(128)
        clientsocket.send(bytes(line,"utf-8"))
    
    #Signal the end of data transfer and close the client socket after data is serviced
    clientsocket.send(bytes('End',"utf-8"))
    file.close()
    clientsocket.close()
    


