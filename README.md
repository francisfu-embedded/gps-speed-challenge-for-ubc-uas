# GPS Speed Challenge For UBC UAS

A Python project containing both a client and a server. 
The server services "real time" GPS data from a txt file, while the client reads the data provided by the server, processes it, and calculates the "real time" instantaneous speed and average speed. 
The two applications should be run at the same time by using two terminals, with the client runned first.
Please make sure that the utm module is pip-installed in your environment before running both applications.
